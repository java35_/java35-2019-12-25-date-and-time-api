package telran.calendar;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;
import java.time.YearMonth;
import java.time.format.TextStyle;
import java.util.Locale;

public class PrintCalendarAppl {

	private static final int DATE_WITH = 4;

	public static void main(String[] args) {
		
		int[] monthYear = args.length == 2 || args.length == 3 
							? getMonthYear(args)
							: getCurrentMonthYear();
		if(monthYear==null)
		{
			System.out.println("Wrong input - usage:<month number> <year number>");
			return;
		}
		int month = monthYear[0];
		int year = monthYear[1];
		
		int dayOfWeek = 1;
		try {
			if (args.length == 3) 
				dayOfWeek = Integer.parseInt(args[2]);
		} catch (Exception e) {
			e.printStackTrace();
		}
		printMonthYear(month, year, dayOfWeek);
	}
	
	private static void printMonthYear(int month, int year, int dayOfWeek) {
		printTitle(month,year);
		printWeekDayNames(dayOfWeek);
		printDates(month, year, dayOfWeek);
		System.out.println();
	}

	private static void printMonthYear(int month, int year) {
		printTitle(month,year);
		printWeekDayNames();
		printDates(month,year);
		System.out.println();
	}
	
	private static void printWeekDayNames() {
		System.out.print("  ");
		for(int i = 1;i<=7;i++)
		{
			System.out.print(getWeekDayName(i) + " ");
		}
		System.out.println();
	}
	
	private static void printWeekDayNames(int dayOfWeek) {
		System.out.print("  ");
		for(int i = 1; i <= 7; i++) {
			String nameOfDay = getWeekDayName(dayOfWeek);
			System.out.print(nameOfDay + " ");
			dayOfWeek = dayOfWeek + 1;
			if (dayOfWeek >= 8)
				dayOfWeek = 1;
		}
		System.out.println();
	}

	private static void printDates(int month, int year) {
		int firstColumn = getFirstColumn(month,year);
		printOffset(firstColumn);
		printNumbersFromOffset(firstColumn,month,year);
	}
	
	private static void printDates(int month, int year, int dayOfWeek) {
		int firstColumn = getFirstColumn(month, year, dayOfWeek);
		printOffset(firstColumn);
		printNumbersFromOffset(firstColumn,month,year);
	}

	private static void printNumbersFromOffset(int firstColumn, int month, int year) {
		int nDays = getNumberOfDays(month,year);
		for(int i = 1;i<=nDays;i++)
		{
			System.out.printf("%"+DATE_WITH+"d",i);//"%4d",i
			if(firstColumn==7)
			{
				firstColumn=1;
				System.out.println();
			}else{
				firstColumn++;
			}
		}
		
	}

	private static int getNumberOfDays(int month, int year) {
		YearMonth ym = YearMonth.of(year, month);
		return ym.lengthOfMonth();
	}

	private static void printOffset(int firstColumn) {
		for(int i = DATE_WITH; i <= firstColumn * DATE_WITH - 1; i++) {
//			 i == 4; 6 * 4 - 1 == 23
			System.out.print(" ");
		}
//		System.out.printf("%" + (DATE_WITH * (firstColumn - 1)) + "s", " ");
		
	}

	private static int getFirstColumn(int month, int year) {
		LocalDate firstDate = LocalDate.of(year, month, 1);
		return firstDate.getDayOfWeek().getValue();
	}
	
	// dOW
	// 7 -> 1 <=> 7 - dOW(7) + 1 == 1
	// 6 -> 2 <=> 7 - dOW(6) + 1 == 2
	// 5 -> 3 <=> 7 - dOW(5) + 1 == 3
	// 4 -> 4
	// ...
	
	private static int getFirstColumn(int month, int year, int dayOfWeek) {
		LocalDate firstDate = LocalDate.of(year, month, 1);
		
		return firstDate.getDayOfWeek().plus(7-dayOfWeek + 1).getValue();
	}


	private static String getWeekDayName(int i) {
		DayOfWeek dayOfWeek = DayOfWeek.of(i);
		return dayOfWeek.getDisplayName(TextStyle.SHORT, Locale.forLanguageTag("en"));
	}

	private static void printTitle(int month, int year) {
		String monthName = getMonthName(month);
		System.out.printf("%6s%s %d\n"," ",monthName,year);
		
	}

	private static String getMonthName(int month) {
		Month enMonth = Month.of(month);
		
		return enMonth.getDisplayName(TextStyle.FULL, 
				Locale.forLanguageTag("en"));
	}

	private static int[] getCurrentMonthYear() {
		LocalDate current = LocalDate.now();
		return new int[]{current.getMonthValue(),current.getYear()};
	}

	private static int[] getMonthYear(String[] args) {
		int[] res = new int[2];
		try {
			res[0] = Integer.parseInt(args[0]);
			res[1] = Integer.parseInt(args[1]);
			if (res[0] < 1 || res[0] > 12)
				return null;
			if (res[1] < 0)
				return null;
		} catch (Exception e) {
			return null;
		}
		return res;
	}

}
