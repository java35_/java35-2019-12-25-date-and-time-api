import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;

public class DateTimeAppl {
	public static void main(String[] args) {
		System.out.println(Instant.EPOCH);
		System.out.println(Instant.now());
		System.out.println(Instant.ofEpochMilli(-1));
		System.out.println(Instant.parse("2019-06-07T00:00:00Z"));
		System.out.println(Instant.from(ZonedDateTime.now()));
		
		Instant inst1 = Instant.now();
		inst1.plus(5, ChronoUnit.DAYS);
		
		Instant inst2 = inst1.plus(5, ChronoUnit.DAYS);
		System.out.println(inst1);
		System.out.println(inst2);
		
//		Duration.between(startInclusive, endExclusive)
		
	}
}
