import java.time.Duration;
import java.time.Instant;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class InstantDurationTest {

	public static void main(String[] args) {
		Instant start1 = Instant.now();
		method1();
		Instant end1 = Instant.now();
		Duration duration1 = Duration.between(start1, end1);
		System.out.println(duration1.toNanos());
		
		
		Instant start2 = Instant.now();
		method2();
		Instant end2 = Instant.now();
		Duration duration2 = Duration.between(start2, end2);
		System.out.println(duration2.toNanos());
		
		System.out.println(duration2.toNanos() / duration1.toNanos());
		
		System.out.println(duration1.multipliedBy(27).minus(duration2).isNegative());
	}
	
	public static void method1() {
		List<Integer> list =
						new Random().ints()
							.limit(100)
							.boxed()
							.collect(Collectors.toList());
		Collections.sort(list);
	}
	
	public static void method2() {
		List<Integer> list =
				new Random().ints()
					.limit(1_000_000)
					.boxed()
					.collect(Collectors.toList());
		Collections.sort(list);
	}

}
